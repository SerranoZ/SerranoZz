## Olá, meu nome é Lucas Serrano SerranoZz

<div align="center" style="display:flex; flex-direction: row;">
  <a href="https://github.com/SerranoZz">
    <img height="180em" src="https://github-readme-stats.vercel.app/api?username=SerranoZz&show_icons=true&theme=dracula&include_all_commits=true&count_private=false"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=SerranoZz&layout=compact&langs_count=7&theme=dracula"/>
</div>

<div style="display: inline_block"><br>
  <img align="center" alt="Lucas-HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="Lucas-CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img align="center" alt="Lucas-Ruby" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/ruby/ruby-plain.svg">
  <img align="center" alt="Lucas-Rails" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/rails/rails-plain.svg">
  <img align="center" alt="Lucas-Python" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="Lucas-C" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/c/c-plain.svg">
  
  <img align="right" alt="Lucas-pic" height="150" style="border-radius:50px;" 
       src="https://i.pinimg.com/474x/45/c5/0a/45c50ad4a3a5ae9a22ee9f57a031863a.jpg">
</div>
  
  ##
 
<div> 
 
  <a href="https://instagram.com/lucasdserrano" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a> 
  <a href = "mailto:lucas_serrano@id.uff.br"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/lucas-serrano-296651225/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
 
 
</div>
  
   ![Snake animation](https://github.com/rafaballerini/rafaballerini/blob/output/github-contribution-grid-snake.svg)

